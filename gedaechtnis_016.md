# Gedachtnis 16
13.07.2019

## Situation
Feier beendet. Alle schlafen im Gasthaus

## Gasthaus
- Ola wacht als erstes uaf
- Türe lässt sich nicht öffnen
- Dagegen werfen - nichts
- Ruft und klopft gegen die Wand zum anderen Zimmer
- Skjorn wacht auf
- Türe ebenfalls zu
- Aufdrücken: geht
- Vor der Türe: Truhen
- Skjorn geht wieder ins Zimmer um sich anzuziehen
- Ola schaut in die Truhen: Voll mit Goldmünzen
  - Münzen haben aber einen Pfau drauf statt dem üblichen Symbol
- Quitschen
- Rabur wacht auf, greift nach seiner Axt und wackelt nach drausen
- "Haben dich die Truhen angegriffen?"
- Nein. Ja. Quasi
- Rabur geht sich auch ordentlich anziehen.
- Geladon inzwischen auch angezogen geht mit Skjorn nach unten
- Keiner da
- Gasthaus verlassen. Essen und Trinken steht alles noch da als ob gerade
  verlassen
- Drausen auch niemand. Und dunkel. Bzw vernebelt.
- Holen Ola und Rabur ab
- Rabur setzt sich 10 Min hin um seine Stärkerune zu aktivieren
- Die anderen schauen drausen im Umkreis nach Leuten: Niemand
- Finden niemanden.
- Als Rabur fertig ist ruft er nochmal so laut er kann nach Leuten. Keine
  Antwort
- Möglichkeiten zum prüfen: Thaumaturgenzelt, Festplatz, Schlagenschrein,
  SChmiedesohn(Stand des Schmieds)
- Auf dem Weg: treffen eine einzige Wache. Benimmt sich wie ein Roboter
- Thaumaturgenzelt: Nicht da. Aber Magische Pilze in Form eines Bannkreises.
- Stand des Schmiedes: Nicht da. Aber ein Stand mit Äpfeln
- Schlangenschrein:
  - Keine Kirche
  - Altar vorhanden. Befreit von Farbe
  - Statt der Kirche ein riesiger Baum dessen Äste energie in nahe Häuser
	 leiten.
  - Skjorn und Rabur klopfen an je einem der Häuser
    - Nette Leute aber verhalten sich auch wie Roboter.
	 - Rabur findet heraus dass sie immernoch im selben Jahr sind
	 - Und man kann einmal am Tag wünsche äußern. Am Schrein oder bei der
		regierenden Familie
		- Wer regiert? Die Familie Pfau
		- Wo? im Schloss am Ende der Stadt
	- Skjorn nimmt das Angebot an Pause zu machen, bekommt etwas zu essen
	  - Bittet um einen Tanz: Dame fängt an einen Schlangentanz zu tanzen
	  - versucht zu bezahlen: Nicht nötig.
  - Gehen zum Schloss
  - Kommen am Festplatz vorbei: Großer See
  - Vorschlag Rabur es gibt Legenden dass Wasserwesen solche Nebel um Stadte und
	 Schiffe legen und dort alle Leute verschwindne.
  - Am See ist eine Weide: Nymphen sollen angeblich an Weiden leben
  - Nymphen lieben laut Sagen hübsches geschmeide
   - Ola formt einen Armreif
	  - Erster versuch: spaltet Bleiklumpen
	  - Zweiter Versuch: rutscht ab und sticht sich in die Hand
	  - Dritter Versuch: klappt
	- Rabur sticht eine verschnörkelte Rune in eine Goldmünze
	- Ola macht die Münze and den Armreif
	- Skjorn nimmt den Armreif und läuft singend auf den See zu bzw in den See
  - Es kommt eine Sirene
  - Findet das Singen schöner als den Armreif
  - Rabur setzt sich und skizziert die Szene
  - Versucht Skjorn zu bezirzen Ihr in den See zu folgen.
  - klappt nicht so gut
  - Rabur: auch nicht erwischt
  - Ola: kann sich ein bisschen gegen Wehren aber will der Sirene folgen
  - Geladon: völlig bezirzrt. Will sofort mit ins Wasser
  - Rabur prollt um die Sirene zu vertreiben.
  - "Was für ein Rüpel"
  - Skjorn spricht mit der Sirene, sagt Ihr er kommt nicht mit ins Wasser weil
	 er ein Wanderer ist aber vieleicht wenn er seine Wanderschaft beendet hat
  - Skjorn schnappt Geladon damit er nicht der Sirene folgt
  - Überlegung: Trotzdrm Nymphen. Hier im See ist sowieso schon alles verkehrt.
	 Falsche Fischmethode mit dem Singen?
  - Fazit: Sirenen sind Gruppenwesen. Nymphen sind Territoriale Einzelgänger.
	 Kein Platz in dem kleinen See
  - Weiter zum Schloss
  - Hier sind wachen aber auch alles Roboter
  - Fällt auf: Auch Frauen dabei
  - Treffen auf Stallburschen
  - Scheint weniger Roboter
