# Gedachtnis 15
11.05.2019
 21
## Situation
Alle sind auf der Feier

## Auf der Feier
- Skjorn soll die Leute ablenken währen Rabur und Kimia die Runen verteilen
- Skjorn verkündet in der nächsten Bardenpause starke Männer sollen Ihn zum
  Armdrücken herausfordern. Wer gewinnt bekommt ein Goldstück. Wer verliert
  zahlt eins.
- Ola macht Werbung für ihn, mit Rufen und Blendsalz
- Menge noch unbegeistert
- Skjorn pickt einen Mann heraus und vordert ihn heraus.
  - Mann unbegeistert. "Warum sollte Ich?"
- Ola versucht eine Gruppe junger Männder zu motivieren, aber auch unbegeistert.
- Geladon macht solange Werbung
- Mann nimmt die Herausforderung an "10 Gold"
- Skjorn zerlegt ihn. Zweimal
- Kommt ein Hämpfling vorbei und fordert ihn heraus.
  - Erster versuch: Skjorn muss richtig arbeiten aber kriegt es hin
  - Zweiter versuch: Wird von ihm zerlegt. Name: Torvin
- Rabur schneidet solange die Runen:
  - 26 Zuberkunde 19
  - 17 Zauberkunde 31 PP 
    - Neu machen mit 24
	   - Zauberkunde 21
  - 30 Zauberkunde 20
  - 19 Zauberkunde 20
    - Neu machen mit 24
	   - Zauberkunde 33 - PP
- Kimia schneidet solange ihre Runen.
- Kimia und Rabur verteilen die äußeren Runen. Rabur trägt die Fässer und sie
  bestimmt wo sie hin kommen
- Danach verteilt Rabut seine Fässer mit den Runen
- Geladon hört den Schrei eines Kindes und will direkt in die Richtung los
- Rabur bemerkt ihn und darauf auch den Schrei
- Rabur zu Geladon:
  - "Wo willst du hin?"
  - "Zum Schrei"
  - "Guter Plan aber sag erst den andern bescheid"
- Rabur verteilt weiter die Fässer
- Geladon sagt Ola bescheid
- Ola kommt mit.
- Ein Junge sitzt schreiend mitten in der Mitte der spielenden Kinder
- Ola geht zu ihm und berührt ihn
- Ola spürt einen kalten Schauer über den Rücken
	- Hand weg: kein Schauer
	- Hand wieder hin: Schauer
	- Hand wieder weg: kein Schauer.
- Frage an Geldadon: kann er damit was anfangen?
- Nein, das ist doch eher Sache der Zwerge
- Ola fragt ein Kind ob der Kleine das schon länger ist.
  - Warum bist du so Klein
  - Bevor 
- Rabur und Kimia bauen erfolgreich das Netz auf.
- Kimia hält es aufrecht.
- Frage wie lang kann sie das aufrecht erhalten: mindestens 30 Momente
- Geht Richtung Schrei
- Kommt an Skjorn vorbei
- Informiert ihn dass die Fässer stehen und über den Schrei
- Früstückt seinen Ritter ab
- Teilt mit dass er pause macht aber später wieder kommt
- Gehen rüber zu dem Kind
- Ola sagt ihnen was sie erlebt hat
- Skjorn prüft die Aura: Seine Aura ist zerstreut
- Rabur prüft wo der Kleine Sitzt:
  - In der Sichtlinie der Vogelstatue
- Rabur nimmt seine Axt und hakt den Schaft in den Grütel des Kleinen und will
  ihn aus der Sichtlinie der Figur tragen
- Er findet sich in einer Schwarzen Welt wieder mit dem Kleinen der schluckzt
- "Was los?"
  - "Er will etwas handeln"
- "Wer ist die Pfeife die hier verhandeln will?"
- Rote Augen überall
- Die Stimme versucht Rabur zu überreden dass sie ihm seine tiefsten Wünsche
  erfüllen.  
  Dafür soll er der Dunkelheit dienen
- Rabur stellt sich Stur:
  - Er wünscht sich dass dieses Wesen nie existiert hat
- Geht nicht
- Das Wesen bietet ihm Geschichten aus "allen" Welten
- Rabur will davon nichts wissen: Er will seine eigene Geschichte schreiben
  - Außerdem wartet sein Stamm in dieser Welt dass er seine Reise beendet und
	 Geschichten dieser Welt mitbringt.
- Der Kleine Lacht und verschwindet
- Die Stimme behauptet der Kleine hat einen Deal mit ihr abgeschlossen zu haben.
- Die Stimme droht Rabur an Ihn hier aleine zu lassen
- Es wird komplett dunkel
- Rabur nimmt an und setzt sich und beginnt die Geschichte zu schreiben seitdem
  er auf die anderen getroffen ist
- Skjorn takelt Rabur vom Kind weg - keine Änderung
- Kind schreit weiter
- Skjorn ohrfeigt Kind - keine Änderung
- Skjorn tippt das Kind mit seinem Stab an
- Das Kind hört auf zu schreien
- Schaut Skjorn an
- Kind: Will zu Papa
- Kind: Jemand hat ihm einen Wunsch erfüllt
- Skjorn: Welcher Wunsch?
- Kind: Darf es nicht sagen
- Skjorn: Wer hat den Wunsch erfüllt?
- Kind: Ein Freund
- Diskussion was ein Freund ist. 
- Skjorn: Macht Rabur den Mund auf nud Bier rein
- Rabur: spuckt es aus
- Skjorn: Ohrfeigt ihn, keine Reaktion
- Rabur: Reinigungsalkohol, in den Mund geklemmt. Hustet
- Skjorn: tippt ihn mit dem Stab ab
- (Rabur: Ein Licht leuchtet auf)
- Skjorn: Wirkt Gottesgabe und dann austreiben von Bösem (Wurf: 37)
- Rabur: bereitet sich vor wenn wieder ein Licht auftaucht es zu betrachten
- Rabur: weiteres Licht: Sieht Skjorn und das Licht is Göttlich und Licht.
- Rabur: Baut eine Runenkreis der sich auf Licht und Göttlicheenergie verbindet
  und eine Rune des Geistes auf seine Stirn um seinen Geist in Richtung der
  Göttlichen Energie katapultiert.
- Weiteres Licht:
  - Es katapultiert Rabur vorwärts und dann blinzelt er
- Erstmal zu sich kommen - was ist das für ein Widerlicher Geschmack?
  - Reingungsalkohol
  - Bier Ausgespuckt: Scheise
- Balg weg
- Rabur erzählt den anderne dass der Kleine einen Deal mit der Stimme
  abgeschlossen hat
- Balg finden
- Skjorn geht zurück zum Stand
- Ola und Geladon suchen auf dem Feld
- Rabur fragt die Kinder
- Kinder wissen nichts davon
- Rabur fragt eins wieviel Taschengeld es kriegt
  - Frage was ist Taschengeld
  - Braucht kein Taschengeld weil Vater alles kauft
- Rabur zeigt ihm die blauen Flammen des Flammenkreis
- Angebot: Wenn er rausfindet wer der Schreihals war malt er dem kleinen 2 der
  Zeichen um den Flammenkreis aktivieren zu können
- Ola: sucht auf der Feier
  - Findet [Lisa](NPCs/Lisa.md)
  - Meint der Junge war Marius, Sohn vom Schmied
- Rabur sagt Kemia bescheid und stabilisiert den Bannkreis mit.
- Rabur wird später nochmal aus dem Kreis geworfen (Patzer)
- In dieser Zeit fangen Leute an sich zu klopfen
- Rabur steigt wieder ein (Krit)
  - Die Leute hören auf sich zu klopfen
- Skjorn geht an den Rand der Feier - außerhalb des Kreises völlige Dunkelheit
- Er entscheidet es ist keine gute Idee Leute gehen zu lassen
- Bittet Hermann und Söhne die Leute möglichst besoffen zu machen dass sie da
  bleiben
- Rabur und Kemia halten bis zum Morgengrauen durch. Rabur klappt aber danach
  zusammen
- Zurück zum Gasthaus

[Back](README.md)
